public class Main {
    public static void main(String[] args) {
        HockeyManager hockeyManager = new HockeyManager();
        hockeyManager.addNewDefender("Martin Fehervary", 23,4);
        hockeyManager.addNewForward("Peter Cehlarik", 27, 8);
        hockeyManager.addNewGoalie("Jaroslav Halak", 37, 10);
        hockeyManager.printNameAndAgeOfTheYoungestPlayer();
    }
}
